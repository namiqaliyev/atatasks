﻿using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.DAL
{
    public class AtaDbContext : DbContext
    {
        public AtaDbContext()
        {

        }

        public AtaDbContext(string csName) : base(csName)
        {

        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Contract> Contacts { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<EnumLookup> EnumLookup { get; set; }
    }
}
