﻿using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.DAL
{
    public interface IDataAccessProvider
    {
        IRepository<T> GetRepository<T>() where T : Entity;
        void SaveChanges();
    }
}
