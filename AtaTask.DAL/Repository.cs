﻿using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.DAL
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected DbContext dbContext;
        protected DbSet<T> collection;

        public Repository(DbContext dbContext)
        {
            this.dbContext = dbContext;
            collection = dbContext.Set<T>();
        }

        public void Delete(T item)
        {
            item.IsAlive = false;
            dbContext.Entry(item).State = EntityState.Modified;
        }

        public T Find(Expression<Func<T, bool>> predicate)
        {
            return collection.FirstOrDefault(predicate);
        }

        public void Insert(T item)
        {
            //item.InsertAt = DateTime.Now;
            //item.ModifiedAt = DateTime.Now;
            //item.IsAlive = true;
            collection.Add(item);
        }

        public void Update(T item)
        {
            item.ModifiedAt = DateTime.Now;
            dbContext.Entry(item).State = EntityState.Modified;
        }

        public IQueryable<T> All()
        {
            return collection;
        }

        public IQueryable<T> Select(Expression<Func<T, bool>> predicate)
        {
            return collection.Where(predicate);
        }
    }
}
