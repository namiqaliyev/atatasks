﻿using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.DAL
{
    public interface IRepository<T> where T: Entity
    {
        IQueryable<T> All();
        IQueryable<T> Select(Expression<Func<T, bool>> predicate);
        T Find(Expression<Func<T, bool>> predicate);
        void Insert(T item);
        void Update(T item);
        void Delete(T item);
    }
}
