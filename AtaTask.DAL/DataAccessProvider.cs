﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtaTask.Entities;

namespace AtaTask.DAL
{
    public class DataAccessProvider : IDataAccessProvider
    {
        public DataAccessProvider()
        {
            DbContext = new AtaDbContext("AtaDbContext");
        }

        public AtaDbContext DbContext { get; private set; }

        public IRepository<T> GetRepository<T>() where T : Entity
        {
            return new Repository<T>(DbContext);
        }

        public void SaveChanges()
        {
            DbContext.SaveChanges();
        }
    }
}
