namespace AtaTask.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contracts",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ContractNumber = c.String(nullable: false, maxLength: 20),
                    ContractDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    Salary = c.Int(nullable: false),
                    Department = c.String(nullable: false, maxLength: 100),
                    PositionId = c.Int(nullable: false),
                    InsertAt = c.DateTime(precision: 7, storeType: "datetime2", defaultValueSql: "GETDATE()"),
                    ModifiedAt = c.DateTime(precision: 7, storeType: "datetime2", defaultValueSql: "GETDATE()"),
                    IsAlive = c.Boolean(defaultValue: true),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployeePositions", t => t.PositionId, cascadeDelete: true)
                .Index(t => t.PositionId);

            CreateTable(
                "dbo.EmployeePositions",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    PositionName = c.String(nullable: false, maxLength: 100),
                    InsertAt = c.DateTime(precision: 7, storeType: "datetime2", defaultValueSql: "GETDATE()"),
                    ModifiedAt = c.DateTime(precision: 7, storeType: "datetime2", defaultValueSql: "GETDATE()"),
                    IsAlive = c.Boolean(defaultValue: true),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Employees",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 20),
                    Surname = c.String(nullable: false, maxLength: 20),
                    BirthDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    Gender = c.Short(nullable: false),
                    Passport = c.String(nullable: false, maxLength: 15),
                    PositionId = c.Int(nullable: false),
                    InsertAt = c.DateTime(precision: 7, storeType: "datetime2", defaultValueSql: "GETDATE()"),
                    ModifiedAt = c.DateTime(precision: 7, storeType: "datetime2", defaultValueSql: "GETDATE()"),
                    IsAlive = c.Boolean(defaultValue: true),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployeePositions", t => t.PositionId, cascadeDelete: true)
                .Index(t => t.PositionId);

            CreateTable(
                "dbo.EnumLookup",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    CategoryId = c.Int(nullable: false),
                    EnumId = c.Int(nullable: false),
                    Value = c.String(),
                    InsertAt = c.DateTime(precision: 7, storeType: "datetime2", defaultValueSql: "GETDATE()"),
                    ModifiedAt = c.DateTime(precision: 7, storeType: "datetime2", defaultValueSql: "GETDATE()"),
                    IsAlive = c.Boolean(defaultValue: true),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Employees", "PositionId", "dbo.EmployeePositions");
            DropForeignKey("dbo.Contracts", "PositionId", "dbo.EmployeePositions");
            DropIndex("dbo.Employees", new[] { "PositionId" });
            DropIndex("dbo.Contracts", new[] { "PositionId" });
            DropTable("dbo.EnumLookup");
            DropTable("dbo.Employees");
            DropTable("dbo.EmployeePositions");
            DropTable("dbo.Contracts");
        }
    }
}
