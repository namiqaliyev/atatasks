namespace AtaTask.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AtaTask.DAL.AtaDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AtaTask.DAL.AtaDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.EnumLookup.AddOrUpdate(new Entities.EnumLookup
            {
                CategoryId = 1,
                EnumId = 1,
                Value = "Male"
            });
            context.EnumLookup.AddOrUpdate(new Entities.EnumLookup
            {
                CategoryId = 1,
                EnumId = 2,
                Value = "Female"
            });
            context.SaveChanges();
        }
    }
}
