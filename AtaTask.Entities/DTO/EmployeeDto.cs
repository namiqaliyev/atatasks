﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Entities.DTO
{
    public class EmployeeDto
    {
        public Employee Employee { get; set; }
        public string GenderText { get; set; }
    }
}
