﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Entities
{
    [Table("Employees")]
    public class Employee : Entity
    {
        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [Required]
        [StringLength(20)]
        public string Surname { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime BirthDate { get; set; }

        [Required]
        public short Gender { get; set; }

        [Required]
        [StringLength(15)]
        public string Passport { get; set; }

        public int PositionId { get; set; }

        public virtual Position Position { get; set; }
    }
}
