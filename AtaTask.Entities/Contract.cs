﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Entities
{
    [Table("Contracts")]
    public class Contract : Entity
    {
        [Required]
        [StringLength(20)]
        public string ContractNumber { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime ContractDate { get; set; }

        [Required]
        public int Salary { get; set; }

        [Required]
        [StringLength(100)]
        public string Department { get; set; }

        public int PositionId { get; set; }

        public virtual Position Position { get; set; }
    }
}
