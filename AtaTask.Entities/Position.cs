﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Entities
{
    [Table("EmployeePositions")]
    public class Position : Entity
    {
        [Required]
        [StringLength(100)]
        public string PositionName { get; set; }
    }
}
