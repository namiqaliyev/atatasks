﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Entities
{
    [Table("EnumLookup")]
    public class EnumLookup : Entity
    {
        public int CategoryId { get; set; }
        public int EnumId { get; set; }
        public string Value { get; set; }
    }
}
