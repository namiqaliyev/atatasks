﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Utils
{
    public static class Mapper
    {
        public static T Map<T>(object o) where T: class
        {
            var output = Activator.CreateInstance<T>();
            var propList = typeof(T).GetProperties().Select(p => p.Name).ToArray();
            foreach(var prop in o.GetType().GetProperties())
            {
                if (propList.Contains(prop.Name))
                {
                    var value = prop.GetValue(o);
                    output.GetType().GetProperty(prop.Name).SetValue(output, value);
                }
            }
            return output;
        }
    }
}
