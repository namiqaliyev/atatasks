﻿using AtaTask.UI.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtaTask.UI.Models.ViewModels
{
    public class ViewModel
    {
        public string PageTitle { get; set; }
        public PageType PageType { get; set; }
    }
}