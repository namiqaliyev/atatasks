﻿using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtaTask.UI.Models.ViewModels.Positions
{
    public class PositionsCreate_ViewModel : ViewModel
    {
        public Position Position { get; set; }
    }
}