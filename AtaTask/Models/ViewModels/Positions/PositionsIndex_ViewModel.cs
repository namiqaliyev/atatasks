﻿using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtaTask.UI.Models.ViewModels.Positions
{
    public class PositionsIndex_ViewModel : ViewModel
    {
        public IEnumerable<Position> Positions { get; set; }
    }
}