﻿using AtaTask.Entities;
using AtaTask.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtaTask.UI.Models.ViewModels.Employees
{
    public class EmployeesIndex_ViewModel : ViewModel
    {
        public IEnumerable<EmployeeDto> Employees { get; set; }
        public IEnumerable<SelectListItem> Positions { get; set; }
        public IEnumerable<SelectListItem> Genders { get; set; }
    }
}