﻿using AtaTask.Entities;
using AtaTask.Entities.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtaTask.UI.Models.ViewModels.Employees
{
    public class EmployeeCreate_ViewModel : ViewModel
    {
        public EmployeeDto Employee { get; set; }
        public IEnumerable<SelectListItem> Positions { get; set; }
        public IEnumerable<SelectListItem> Genders { get; set; }
    }
}