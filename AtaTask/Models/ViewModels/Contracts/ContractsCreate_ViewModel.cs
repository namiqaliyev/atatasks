﻿using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtaTask.UI.Models.ViewModels.Contracts
{
    public class ContractsCreate_ViewModel: ViewModel
    {
        public Contract Contract { get; set; }
        public IEnumerable<SelectListItem> Positions { get; set; }
    }
}