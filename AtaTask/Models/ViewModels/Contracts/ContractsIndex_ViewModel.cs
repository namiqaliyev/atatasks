﻿using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtaTask.UI.Models.ViewModels.Contracts
{
    public class ContractsIndex_ViewModel : ViewModel
    {
        public IEnumerable<Contract> Contracts { get; set; }
    }
}