﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtaTask.UI.Custom
{
    public abstract class BaseController : Controller
    {
        protected JsonResult Success(object data = null)
        {
            //Response.StatusCode = 200;
            return Json(new {
                hasError = false,
                data = data
            });
        }

        protected JsonResult Failure(object data = null)
        {
            //Response.StatusCode = 404;
            return Json(new {
                hasError = true,
                data = data
            });
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            var msg = filterContext.Exception.Message;
            filterContext.ExceptionHandled = true;

            if (Request.IsAjaxRequest())
                filterContext.Result = Failure(new
                {
                    message = msg
                });
            else
            {
                var viewResult = new ViewResult
                {
                    ViewName = "Error"
                };
                viewResult.TempData["msg"] = msg;
                filterContext.Result = viewResult;
            }

            //base.OnException(filterContext);
        }

        protected IEnumerable<string> GetModelStateErrors()
        {
            var list = new List<string>();
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    list.Add(error.ErrorMessage);
                }
            }

            return list;
        }

        protected string ViewAsString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}