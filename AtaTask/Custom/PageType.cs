﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtaTask.UI.Custom
{
    public enum PageType
    {
        None,
        List,
        CreateOrEdit,
        Delete
    }
}