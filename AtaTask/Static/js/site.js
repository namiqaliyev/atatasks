﻿function submit_form(e) {
    e.preventDefault();
    var $target = $(e.target);
    var target_form = $target.data("target-form");
    var form = document.getElementById(target_form);
    form = $(form);
    var data = form.serialize();
    var url = form.attr("action");
    $target.html("<i class='fa fa-refresh fa-spin'></i>&nbsp;Saving...").attr("disabled","disabled");
    $.post(url, data, function (response) {
        $target.html("<i class='fa fa-save'></i>&nbsp;Save").removeAttr("disabled");
        if (response.hasError) {
            bootbox.alert(response.data.message);
        } else {               
            bootbox.alert("Data saved successifully");
        }
    });
}

function remove(e) {
    e.preventDefault();
    var $target = $(e.target);
    var target_form = $target.data("target-form");
    var form = document.getElementById(target_form);
    form = $(form);
    var data = form.serialize();
    var url = form.attr("action");
    $target.html("<i class='fa fa-refresh fa-spin'></i>&nbsp;Deleting...").attr("disabled", "disabled");
    $.post(url, data, function (response) {
        if (response.hasError) {
            $target.html("<i class='fa fa-remove'></i>&nbsp;Remove").removeAttr("disabled");
            bootbox.alert(response.data.message);
        } else {
            document.location = response.data.url;
        }
    });
}

function search(e, clear) {
    e.preventDefault();
    var clear = clear || false;
    var elem = e.target;
    var target_form_id = $(elem).data("target-form");
    var form = document.forms[target_form_id];
    if (clear)
        form.reset();

    target_form_id = "#" + target_form_id;
    var target_div = "#" + $(target_form_id).data("target-div");
    var search_summary_id = "#" + $(target_form_id).data("search-summary");
    var url = $(target_form_id).attr("action");
    var data = $(form).serialize();

    $.post(url, data, function (response) {
        $(target_div).html(response.data.htmlContent);
        if (clear) {
            $(search_summary_id).empty();
        } else {
            $(search_summary_id).html("[Search result: " + response.data.rowCount + "]");
        }
    });
}
