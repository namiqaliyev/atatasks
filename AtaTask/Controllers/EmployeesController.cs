﻿using AtaTask.Entities;
using AtaTask.Entities.DTO;
using AtaTask.Services;
using AtaTask.Services.Inputs;
using AtaTask.UI.Custom;
using AtaTask.UI.Models.ViewModels.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtaTask.UI.Controllers
{
    public class EmployeesController : BaseController
    {
        private readonly EmployeesService employeeService;
        private readonly PositionsService positionsService;
        private readonly EnumLookupService enumService;

        public EmployeesController()
        {
            employeeService = new EmployeesService();
            positionsService = new PositionsService();
            enumService = new EnumLookupService();
        }

        public ActionResult Index()
        {
            var employees = employeeService.AllAliveEmployees();
            var positions = positionsService.AllAlivePositions();
            var enums = enumService.AllAliveEnums();

            var viewModel = new EmployeesIndex_ViewModel
            {
                PageTitle = "Employees",
                PageType = Custom.PageType.List,
                Employees = employees,
                Positions = positions.Select(p => new SelectListItem
                {
                    Text = p.PositionName,
                    Value = p.Id.ToString()
                }).ToArray(),
                Genders = enums.Select(p => new SelectListItem
                {
                    Text = p.Value,
                    Value = p.EnumId.ToString()
                }).ToArray()
            };
            return View(viewModel);
        }

        public ActionResult Create(int id = 0)
        {
            var employee = employeeService.GetEmployeeById(id);
            var positions = positionsService.AllAlivePositions();
            var enums = enumService.AllAliveEnums();

            if (id != 0 && employee == null)
            {
                throw new Exception("Data not found");
            }
            var viewModel = new EmployeeCreate_ViewModel
            {
                PageTitle = id == 0 ? "New Employee" : "Edit Employee",
                PageType = Custom.PageType.CreateOrEdit,
                Employee = employee ?? new EmployeeDto() { Employee = new Employee { BirthDate = DateTime.Now} },
                Positions = positions.Select(p => new SelectListItem
                {
                    Text = p.PositionName,
                    Value = p.Id.ToString(),
                    Selected = employee == null ? false : p.Id == employee.Employee.Position.Id
                }).ToArray(),
                Genders = enums.Select(p => new SelectListItem
                {
                    Text = p.Value,
                    Value = p.EnumId.ToString(),
                    Selected = employee == null ? false : p.EnumId == employee.Employee.Gender
                }).ToArray()
            };

            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Save(EmployeeSave_Input input)
        {
            if (ModelState.IsValid)
            {
                employeeService.Save(input);
                return Success();
            }

            var errors = GetModelStateErrors();
            return Failure(new
            {
                message = errors.ElementAt(0)
            });
        }

        public ActionResult Remove(int id)
        {
            var employee = employeeService.GetEmployeeById(id);
            if (employee == null)
            {
                throw new Exception("Page not found");
            }

            var viewModel = new EmployeeCreate_ViewModel
            {
                PageTitle = "Delete employee?",
                PageType = PageType.Delete,
                Employee = employee
            };
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken, ActionName("Remove")]
        public JsonResult RemovePost(int id)
        {
            employeeService.RemoveById(id);
            return Success(new
            {
                url = Url.Action("Index")
            });
        }

        [HttpPost]
        public ActionResult Search(EmployeeSearch_Input input)
        {
            var employees = employeeService.Search(input);

            var result = new
            {
                htmlContent = ViewAsString("_Grid", employees),
                rowCount = employees.Count()
            };

            return Success(result);
        }
    }
}