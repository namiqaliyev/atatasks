﻿using AtaTask.Services;
using AtaTask.Services.Inputs;
using AtaTask.UI.Custom;
using AtaTask.UI.Models.ViewModels.Contracts;
using System;
using System.Linq;
using System.Web.Mvc;

namespace AtaTask.UI.Controllers
{
    public class ContractsController : BaseController
    {
        private readonly ContractsService contractsService;
        private readonly PositionsService positionService;

        public ContractsController()
        {
            contractsService = new ContractsService();
            positionService = new PositionsService();
        }

        public ActionResult Index()
        {
            var contracts = contractsService.AllAliveContracts();
            var viewModel = new ContractsIndex_ViewModel
            {
                PageTitle = "Contracts",
                PageType = PageType.List,
                Contracts = contracts
            };
            return View(viewModel);
        }

        public ActionResult Create(int id = 0)
        {
            var contract = contractsService.GetContractById(id);
            var positions = positionService.AllAlivePositions();
            if (id != 0 && contract == null)
            {
                throw new Exception("Page not found");
            }

            var viewModel = new ContractsCreate_ViewModel
            {
                PageTitle = id == 0 ? "New contract" : "Edit contract",
                PageType = PageType.CreateOrEdit,
                Contract = contract ?? new Entities.Contract() { ContractDate = DateTime.Now },
                Positions = positions.Select(p => new SelectListItem
                {
                    Text = p.PositionName,
                    Value = p.Id.ToString(),
                    Selected = contract == null ? false : p.Id == contract.Position.Id
                }).ToArray()
            };
            return View(viewModel);
        }

        public ActionResult Remove(int id)
        {
            var contract = contractsService.GetContractById(id);
            if (contract == null)
            {
                throw new Exception("Page not found");
            }

            var viewModel = new ContractsCreate_ViewModel
            {
                PageTitle = "Delete contract?",
                PageType = PageType.Delete,
                Contract = contract
            };
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult Save(ContractSave_Input input)
        {
            if (ModelState.IsValid)
            {
                contractsService.Save(input);
                return Success();
            }

            var errors = GetModelStateErrors();
            return Failure(new
            {
                message = errors.ElementAt(0)
            });
        }

        [HttpPost, ValidateAntiForgeryToken, ActionName("Remove")]
        public JsonResult RemovePost(int id)
        {
            contractsService.RemoveById(id);
            return Success(new
            {
                url = Url.Action("Index")
            });
        }
    }
}