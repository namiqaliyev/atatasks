﻿using AtaTask.Services;
using AtaTask.Services.Inputs;
using AtaTask.UI.Custom;
using AtaTask.UI.Models.ViewModels.Positions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtaTask.UI.Controllers
{
    public class PositionsController : BaseController
    {
        private readonly PositionsService positionsService;

        public PositionsController()
        {
            positionsService = new PositionsService();
        }

         public ActionResult Index()
        {
            var viewModel = new PositionsIndex_ViewModel
            {
                PageTitle = "Positions",
                PageType = PageType.List,
                Positions = positionsService.AllAlivePositions()
            };
            return View(viewModel);
        }
        
        public ActionResult Create(int id = 0)
        {
            var position = positionsService.GetPositionById(id);
            if(id != 0 && position == null)
            {
                throw new Exception("Data not found");
            }

            var viewModel = new PositionsCreate_ViewModel
            {
                PageTitle = id == 0 ? "New position" : "Edit position",
                PageType = PageType.CreateOrEdit,
                Position = position ?? new Entities.Position()
            };
            return View(viewModel);
        }

        public ActionResult Remove(int id)
        {
            var position = positionsService.GetPositionById(id);
            if (position == null)
            {
                throw new Exception("Page not found");
            }

            var viewModel = new PositionsCreate_ViewModel
            {
                PageTitle = "Delete position?",
                PageType = PageType.Delete,
                Position = position
            };
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult Save(PositionSave_Input input)
        {
            if (ModelState.IsValid)
            {
                positionsService.Save(input);
                return Success();
            }

            var errors = GetModelStateErrors();
            return Failure(errors.ElementAt(0));
        }

        [HttpPost, ValidateAntiForgeryToken, ActionName("Remove")]
        public JsonResult RemovePost(int id)
        {
            positionsService.RemoveById(id);
            return Success(new
            {
                url = Url.Action("Index")
            });
        }
    }
}