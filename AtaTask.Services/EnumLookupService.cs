﻿using AtaTask.DAL;
using AtaTask.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Services
{
    public class EnumLookupService
    {
        private readonly IDataAccessProvider dap;
        private readonly IRepository<EnumLookup> enumRepository;

        public EnumLookupService()
        {
            dap = new DataAccessProvider();
            enumRepository = dap.GetRepository<EnumLookup>();
        }

        public IEnumerable<EnumLookup> AllAliveEnums()
        {
            return enumRepository.Select(p => p.IsAlive.Value).ToArray();
        }
    }
}
