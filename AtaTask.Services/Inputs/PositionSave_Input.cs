﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Services.Inputs
{
    public class PositionSave_Input
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Position name is required")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Position name must be minimum 3 and maximum 100 characters.")]
        public string PositionName { get; set; }
    }
}
