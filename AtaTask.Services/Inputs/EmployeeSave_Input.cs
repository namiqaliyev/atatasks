﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Services.Inputs
{
    public class EmployeeSave_Input
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [Required]
        [StringLength(20)]
        public string Surname { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public short Gender { get; set; }

        [Required]
        [StringLength(15)]
        public string Passport { get; set; }

        [Required]
        public int PositionId { get; set; }
    }
}
