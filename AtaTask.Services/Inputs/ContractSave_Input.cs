﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Services.Inputs
{
    public class ContractSave_Input
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Contract number is empty")]
        [StringLength(20, ErrorMessage = "Contract number must be maximum 20 characters")]
        public string ContractNumber { get; set; }

        [Required(ErrorMessage = "Contract date is empty")]
        public DateTime ContractDate { get; set; }

        [Required(ErrorMessage = "Salary is empty")]
        public int Salary { get; set; }

        [Required(ErrorMessage = "Department is empty")]
        [StringLength(100, ErrorMessage = "Department must be maximum 100 characters")]
        public string Department { get; set; }

        [Required(ErrorMessage = "Position is not selected")]
        public int PositionId { get; set; }
    }
}
