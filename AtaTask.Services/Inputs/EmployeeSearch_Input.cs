﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Services.Inputs
{
    public class EmployeeSearch_Input
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Passport { get; set; }
        public DateTime? BirthDate { get; set; }
        public short Gender { get; set; }
        public int PositionId { get; set; }
    }
}
