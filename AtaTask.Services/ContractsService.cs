﻿using AtaTask.DAL;
using AtaTask.Entities;
using AtaTask.Services.Inputs;
using AtaTask.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Services
{
    public class ContractsService
    {
        private readonly DataAccessProvider dap;
        private readonly IRepository<Contract> contractsRepository;

        public ContractsService()
        {
            dap = new DataAccessProvider();
            contractsRepository = dap.GetRepository<Contract>();
        }

        public IEnumerable<Contract> AllAliveContracts()
        {
            return contractsRepository.Select(p => p.IsAlive.Value).AsEnumerable();
        }

        public Contract GetContractById(int id)
        {
            return contractsRepository.Find(p => p.Id == id);
        }

        public void RemoveById(int id)
        {
            var contract = contractsRepository.Find(p => p.Id == id);
            contract.IsAlive = false;
            contractsRepository.Delete(contract);
            dap.SaveChanges();
        }

        public void Save(ContractSave_Input input)
        {
            var contract = Mapper.Map<Contract>(input);
            if (input.Id == 0)
            {
                contractsRepository.Insert(contract);
            }
            else
            {
                contractsRepository.Update(contract);
            }
            dap.SaveChanges();
        }
    }
}
