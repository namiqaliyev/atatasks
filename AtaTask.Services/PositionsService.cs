﻿using AtaTask.DAL;
using AtaTask.Entities;
using AtaTask.Services.Inputs;
using AtaTask.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Services
{
    public class PositionsService
    {
        private readonly DataAccessProvider dap;
        private readonly IRepository<Position> positionRepository;

        public PositionsService()
        {
            dap = new DataAccessProvider();
            positionRepository = dap.GetRepository<Position>();
        }

        public IEnumerable<Position> AllAlivePositions()
        {
            return positionRepository.Select(p => p.IsAlive.Value).AsEnumerable();
        }

        public Position GetPositionById(int id)
        {
            return positionRepository.Find(p => p.Id == id);
        }

        public void Save(PositionSave_Input input)
        {
            var position = Mapper.Map<Position>(input);
            if (input.Id == 0)
            {
                positionRepository.Insert(position);
            }
            else
            {
                positionRepository.Update(position);
            }
            dap.SaveChanges();
        }

        public void RemoveById(int id)
        {
            var position = positionRepository.Find(p => p.Id == id);
            positionRepository.Delete(position);
            dap.SaveChanges();
        }
    }
}
