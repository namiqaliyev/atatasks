﻿using AtaTask.DAL;
using AtaTask.Entities;
using AtaTask.Entities.DTO;
using AtaTask.Services.Inputs;
using AtaTask.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtaTask.Services
{
    public class EmployeesService
    {
        private readonly DataAccessProvider dap;
        private IRepository<Employee> employeesReository;
        private IRepository<EnumLookup> enumRepository;

        public EmployeesService()
        {
            dap = new DataAccessProvider();
            employeesReository = dap.GetRepository<Employee>();
            enumRepository = dap.GetRepository<EnumLookup>();
        }

        public IEnumerable<EmployeeDto> AllAliveEmployees()
        {
            var list = GetAliveEmployees();
            return list.ToArray();
        }

        public EmployeeDto GetEmployeeById(int id)
        {
            //var list = from emp in employeesReository.All()
            //           join enm in enumRepository.All()
            //           on emp.Gender equals enm.EnumId
            //           where emp.Id == id && emp.IsAlive.Value && enm.IsAlive.Value && enm.CategoryId == 1
            //           select new EmployeeDto
            //           {
            //               Employee = emp,
            //               GenderText = enm.Value
            //           };
            var list = GetAliveEmployees().Where(p => p.Employee.Id == id);
            return list.FirstOrDefault();
        }

        public void Save(EmployeeSave_Input input)
        {
            var employee = Mapper.Map<Employee>(input);
            employee.IsAlive = true;

            if (input.Id == 0)
            {
                employeesReository.Insert(employee);
            }
            else
            {
                employeesReository.Update(employee);
            }
            dap.SaveChanges();
        }

        public void RemoveById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EmployeeDto> Search(EmployeeSearch_Input input)
        {
            var result = GetAliveEmployees();

            if (input.Name.IsNotEmpty())
                result = result.Where(p => p.Employee.Name.Contains(input.Name));
            if (input.Surname.IsNotEmpty())
                result = result.Where(p => p.Employee.Surname.Contains(input.Surname));
            if (input.Passport.IsNotEmpty())
                result = result.Where(p => p.Employee.Passport.Contains(input.Passport));
            if (input.BirthDate != null)
                result = result.Where(p => p.Employee.BirthDate == input.BirthDate);
            if (input.Gender != 0)
                result = result.Where(p => p.Employee.Gender == input.Gender);
            if (input.PositionId != 0)
                result = result.Where(p => p.Employee.PositionId == input.PositionId);

            return result.ToArray();
        }

        private IQueryable<EmployeeDto> GetAliveEmployees()
        {
            var list = from emp in employeesReository.All()
                       join enm in enumRepository.All()
                       on emp.Gender equals enm.EnumId
                       where emp.IsAlive.Value && enm.IsAlive.Value && enm.CategoryId == 1
                       select new EmployeeDto
                       {
                           Employee = emp,
                           GenderText = enm.Value
                       };
            return list;
        }
    }
}
